import Part from "./Part";
import Total from "./Total";

const Content = ({ name, parts }) => {
    return (
        <>
            <h2>{name}</h2>
            {
                parts.map(p =>
                    <Part key={p.id} part={p} />
                )
            }
            <Total parts={parts} />
        </>
    )
}

export default Content;