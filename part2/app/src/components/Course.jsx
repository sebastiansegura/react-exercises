import Content from "./Content";

const Course = ({ course }) => {
    return <Content name={course.name} parts={course.parts} />
}

export default Course;