import { useState } from 'react'
import Filter from './components/Filter'
import Results from './components/Results'

function App() {
  const [countries, setCountries] = useState([])
  const [filterCountry, setFilterCountry] = useState('')
  const [countryToShow, setCountryToShow] = useState(null)

  return (
    <div>
      <Filter setCountries={setCountries} filterCountry={filterCountry} setFilterCountry={setFilterCountry}
        setCountryToShow={setCountryToShow} />
      <Results countries={countries} countryToShow={countryToShow} setCountryToShow={setCountryToShow} />
    </div>
  )
}

export default App
