import axios from 'axios'
import { useEffect } from 'react'

const Filter = ({ setCountries, filterCountry, setFilterCountry, setCountryToShow }) => {
    useEffect(() => {
        if(filterCountry !== ''){
            axios.get(`https://restcountries.com/v3.1/name/${filterCountry}`).then(response => {
                setCountries(response.data)
                setCountryToShow(null)
            }).catch(e => {
                setCountries([])
            })
        }
    }, [filterCountry])

    return (
        <p>find countries<input type='text' onChange={() => setFilterCountry(event.target.value)} /></p>
    )
}

export default Filter
