import { useEffect, useState } from 'react'
import axios from 'axios'

const Country = ({ countryToShow, setCountryToShow, buttonBack }) => {
    const [weather, setWheater] = useState(null)

    useEffect(() => {
        axios.get(`http://api.weatherstack.com/current?access_key=${import.meta.env.VITE_WHEATERSTACK_API_KEY}&query=${countryToShow.name.common}`)
            .then(response => {
                setWheater(response.data.current)
            }).catch(e => {
                return null
            })
    }, [])

    return (
        <>
            <h1>{countryToShow.name.common}</h1>
            {countryToShow.capital.length > 0 ? <p>capital {countryToShow.capital[0]}</p> : null}
            <p>population {countryToShow.population}</p>
            <h2>languages</h2>
            {
                Object.entries(countryToShow.languages).length > 0
                    ?
                    <ul>
                        {Object.entries(countryToShow.languages).map(l => <li key={l[0]}>{l[1]}</li>)}
                    </ul>
                    : null
            }
            <img src={countryToShow.flags.png} alt="flag" /> <br />
            <h2>Wheater in {countryToShow.name.common}</h2>
            {
                weather
                    ?
                    <>
                        <p><strong>temperature:</strong> {weather.temperature} Celcius</p>
                        <img src={weather.weather_icons} alt="weather" /> <br />
                        <p><strong>wind:</strong> {weather.wind_speed} mph direction {weather.wind_dir}</p>
                    </>
                    : null
            }
            {buttonBack ? <button onClick={() => setCountryToShow(null)}>back</button> : null}
        </>
    )
}

export default Country
