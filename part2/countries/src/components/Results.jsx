import Country from "./Country"

const Results = ({ countries, countryToShow, setCountryToShow }) => {
    if (countries.length === 0) {
        return null
    } else if (countries.length === 1) {
        return <Country countryToShow={countries[0]} setCountryToShow={setCountryToShow} buttonBack={false} />
    } else if (countries.length > 10) {
        return <p>Too many matches, specify another filter</p>
    } else {
        if (countryToShow) {
            return <Country country={countryToShow} countryToShow={countryToShow} setCountryToShow={setCountryToShow} buttonBack={true} />
        } else {
            return (
                <ul>
                    {
                        countries.map(c => {
                            return <li key={c.name.official}>{c.name.common} <button onClick={() => { setCountryToShow(c) }}>show</button></li>
                        })
                    }
                </ul>
            )
        }
    }
}

export default Results
