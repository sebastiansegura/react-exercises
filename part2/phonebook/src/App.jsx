import React, { useEffect, useState } from 'react'
import Filter from './components/Filter'
import PersonForm from './components/PersonForm'
import Persons from './components/Persons'
import Notification from './components/Notification'
import axios from 'axios'

const App = () => {
  const [persons, setPersons] = useState([])
  const [newName, setNewName] = useState('')
  const [newNumber, setNewNumber] = useState('')
  const [filterName, setFilterName] = useState('')
  const [notification, setNotification] = useState(null)

  useEffect(() => {
    axios.get(import.meta.env.VITE_URL_BACKEND).then(response => setPersons(response.data))
  }, [])

  return (
    <div>
      <h2>Phonebook</h2>
      {
        notification
          ? <Notification notification={notification} />
          : null
      }
      <Filter filterName={filterName} setFilterName={setFilterName} />
      <h2>add a new</h2>
      <PersonForm persons={persons} setPersons={setPersons} newName={newName} setNewName={setNewName}
        newNumber={newNumber} setNewNumber={setNewNumber} notfication={notification} setNotification={setNotification} />
      <h2>Numbers</h2>
      <Persons filterName={filterName} persons={persons} setPersons={setPersons} notification={notification} setNotification={setNotification} />
    </div>
  )
}

export default App