import React from "react";

const Notification = ({ notification }) => {
    let notificationStyles = {
        backgroundColor: '#DCDCDC',
        borderRadius: '.5rem',
        fontWeight: 'bold',
        margin: '1rem',
        padding: '1rem'
    }
    if (notification.type === 'error') {
        notificationStyles.color = 'red'
        notificationStyles.border = '.3rem solid red'
        notificationStyles.borderColor = 'red'
    } else {
        notificationStyles.color = 'green'
        notificationStyles.border = '.3rem solid green'
        notificationStyles.borderColor = 'green'
    }
    return (
        <div style={notificationStyles}>{notification.message}</div>
    );
};

export default Notification;
