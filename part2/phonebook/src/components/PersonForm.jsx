import React from "react";
import personService from "../services/persons";

const PersonForm = ({
  persons,
  setPersons,
  newName,
  setNewName,
  newNumber,
  setNewNumber,
  notification,
  setNotification
}) => {
  const showNotification = (message, type) => {
    setNotification({ ...notification, message, type })
    setTimeout(function () {
      setNotification(null)
    }, 3000);
  }

  const addPerson = (event) => {
    event.preventDefault();
    const existPerson = persons.find(p => p.name === newName);
    if (existPerson) {
      if (window.confirm(`${newName} is already added to phonebook, replace the old number with a new one?`)) {
        personService.updateNumberPerson({ id: existPerson.id, name: newName, number: newNumber })
          .then(person => {
            showNotification(`${newName} number updated`, 'success')
            setPersons(persons.map(p => p.id === person.id ? person : p))
          })
          .catch(e => {
            if (e.response) {
              showNotification(e.response.data.error, 'error')
            } else {
              showNotification(e.message, 'error')
            }
          })
      }
    } else {
      personService.addPerson({ name: newName, number: newNumber })
        .then(person => {
          showNotification(`${newName} added`, 'success')
          setPersons(persons.concat(person))
        })
        .catch(e => {
          if (e.response) {
            showNotification(e.response.data.error, 'error')
          } else {
            showNotification(e.message, 'error')
          }
        })
    }
    setNewName("");
    setNewNumber("");
  };

  return (
    <form onSubmit={addPerson}>
      <div>
        name
        <input
          type="text"
          value={newName}
          onChange={() => setNewName(event.target.value)}
        />
      </div>
      <div>
        number
        <input
          type="text"
          value={newNumber}
          onChange={() => setNewNumber(event.target.value)}
        />
      </div>
      <div>
        <button type="submit">add</button>
      </div>
    </form>
  );
};

export default PersonForm;
