import React from 'react'
import personService from '../services/persons'

const Persons = ({ filterName, persons, setPersons, notification, setNotification }) => {
    const showNotification = (message, type) => {
        setNotification({ ...notification, message, type })
        setTimeout(function () {
            setNotification(null)
        }, 3000);
    }

    const removePerson = (p) => {
        if (window.confirm(`Delete ${p.name} ?`)) {
            personService.deletePerson(p.id)
                .then(response => {
                    showNotification(`${p.name} removed`, 'success')
                    setPersons(persons.filter(person => person.id != p.id))
                })
                .catch(e => {
                    if (e.response) {
                        showNotification(e.response.data.error, 'error')
                    } else {
                        showNotification(e.message, 'error')
                    }
                })
        }
    }

    return (
        <ul>
            {
                filterName === ''
                    ? persons.map(p => <li key={p.id}>{p.name} {p.number} <button onClick={() => removePerson(p)}>delete</button></li>)
                    : persons.filter(p => p.name.toUpperCase().includes(filterName)).map(p => <li key={p.id}>{p.name} {p.number} <button onClick={() => removePerson(p)}>delete</button></li>)
            }
        </ul>
    )
}

export default Persons