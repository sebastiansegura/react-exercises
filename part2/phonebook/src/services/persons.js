import axios from "axios";

const addPerson = (newPerson) => {
  return axios
    .post(import.meta.env.VITE_URL_BACKEND, newPerson)
    .then((response) => response.data)
};

const deletePerson = (id) => {
  return axios
    .delete(import.meta.env.VITE_URL_BACKEND + id)
    .then((response) => response.data)
};

const updateNumberPerson = (updatePerson) => {
  return axios
    .put(import.meta.env.VITE_URL_BACKEND + updatePerson.id, updatePerson)
    .then((response) => response.data)
};

export default { addPerson, deletePerson, updateNumberPerson };
