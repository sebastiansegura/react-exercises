import React from 'react'

const AnecdoteMostVotes = ({ anecdoteMostVotes }) => {
    return (
        <div>
            <h1>Anecdote with most votes</h1>
            {anecdoteMostVotes.anecdote} <br />
            <p>has {anecdoteMostVotes.votes} votes</p>
        </div>
    )
}

export default AnecdoteMostVotes;