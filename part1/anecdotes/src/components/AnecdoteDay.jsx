import React from 'react'

const AnecdoteDay = ({ anecdotes, selected, setSelected, votes, setVotes, setAnecdoteMostVotes }) => {
    const addVote = (position) => {
        const copyVotes = { ...votes }
        copyVotes[position] += 1
        setVotes(copyVotes)
        const anecdoteWithMostVotes = Object.keys(copyVotes).reduce((a, b) => copyVotes[a] > copyVotes[b] ? a : b);
        setAnecdoteMostVotes({
            anecdote: anecdotes[anecdoteWithMostVotes],
            votes: copyVotes[anecdoteWithMostVotes]
        })
    }

    return (
        <div>
            <h1>Anecdote of the day</h1>
            {anecdotes[selected]} <br />
            <p>has {votes === null ? 0 : votes[selected]} votes</p>
            <button onClick={() => addVote(selected)}>vote</button>
            <button onClick={() => setSelected(Math.floor(Math.random() * anecdotes.length))}>next anecdotes</button>
        </div>
    )
}

export default AnecdoteDay;