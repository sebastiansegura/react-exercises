import React, { useState } from 'react'
import AnecdoteDay from './components/AnecdoteDay'
import AnecdoteMostVotes from './components/AnecdoteMostVotes'

const App = ({ anecdotes }) => {
  const [selected, setSelected] = useState(0)
  const [votes, setVotes] = useState(Array(anecdotes.length).fill(0))
  const [anecdoteMostVotes, setAnecdoteMostVotes] = useState({
    anecdote: anecdotes[0],
    votes: votes[0]
  })

  return (
    <>
      <AnecdoteDay anecdotes={anecdotes} selected={selected} setSelected={setSelected} votes={votes} setVotes={setVotes}
      setAnecdoteMostVotes={setAnecdoteMostVotes} />
      <AnecdoteMostVotes anecdoteMostVotes={anecdoteMostVotes} />
    </>
  )
}

export default App;