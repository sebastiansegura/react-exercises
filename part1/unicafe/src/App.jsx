import React, { useState } from 'react'
import Header from './components/Header'
import Statistics from './components/Statistics'

const App = () => {
  // save clicks of each button to its own state
  const [good, setGood] = useState(0)
  const [neutral, setNeutral] = useState(0)
  const [bad, setBad] = useState(0)

  return (
    <div style={{ margin: 10 }}>
      <Header good={good} setGood={setGood} neutral={neutral} setNeutral={setNeutral} bad={bad} setBad={setBad} />
      <h1>statistics</h1>
      {
        good === 0 && neutral === 0 && bad === 0
          ? <p>No feedback given</p>
          : (
            <>
              <Statistics good={good} neutral={neutral} bad={bad} />
            </>
          )
      }
    </div>
  )
}

export default App;