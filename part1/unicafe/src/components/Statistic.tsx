import React from 'react'

const Statistic = ({ text, value }) => {
    return (
        <tr>
            <td key={text}>{text} {text === 'positive' ? value + '%' : value}</td>
        </tr>
    )
}

export default Statistic;