import React from 'react'

const Button = ({ text, counter, setCounter }) => {
    return (
        <button onClick={() => { setCounter(counter + 1) }}>{text}</button>
    )
}

export default Button;