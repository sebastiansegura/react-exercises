import React from 'react'
import Button from './Button'

const Header = ({ good, setGood, neutral, setNeutral, bad, setBad }) => {
    return (
        <div>
            <h1>give feedback</h1>
            <div>
                <Button text={"good"} counter={good} setCounter={setGood} />
                <Button text={"neutral"} counter={neutral} setCounter={setNeutral} />
                <Button text={"bad"} counter={bad} setCounter={setBad} />
            </div>
        </div>
    )
}

export default Header;