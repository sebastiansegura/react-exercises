const bcrypt = require('bcrypt')
const userRouter = require('express').Router()
const User = require('../models/user')

userRouter.get('/', async (request, response) => {
  const user = await User.find({}).populate('blogs', {
    url: 1,
    title: 1,
    author: 1,
    id: 1,
  })
  response.json(user)
})

userRouter.post('/', async (request, response) => {
  const { body } = request

  if (!body.password || body.password.length < 3) {
    return response.status(400).json({
      error: 'password required or invalid',
    })
  }

  const saltRound = 10
  const passwordHash = await bcrypt.hash(body.password, saltRound)

  const user = new User({
    username: body.username,
    name: body.name,
    passwordHash,
  })

  const userSaved = await user.save()
  return response.status(201).json(userSaved)
})

module.exports = userRouter
