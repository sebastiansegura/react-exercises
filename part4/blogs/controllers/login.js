const loginRouter = require('express').Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const User = require('../models/user')

loginRouter.post('/', async (request, response) => {
  const { body } = request
  if (!body.username || !body.password) {
    response.status(400).send({
      error: 'username and password are required',
    })
  } else {
    const user = await User.findOne({ username: body.username })
    const passwordCorrect = user === null ? false : await bcrypt.compare(body.password, user.passwordHash)
    if (!user || !passwordCorrect) {
      response.status(400).send({
        error: 'user or password incorrect',
      })
    } else {
      const userForToken = {
        username: user.username,
        id: user._id,
      }
      const token = jwt.sign(userForToken, process.env.SECRET)
      response.status(200).send({
        token,
        username: user.username,
        name: user.name,
      })
    }
  }
})

module.exports = loginRouter
