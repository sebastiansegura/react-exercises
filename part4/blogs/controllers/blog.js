const blogRouter = require('express').Router()
const Blog = require('../models/blog')
const User = require('../models/user')

blogRouter.get('/', async (request, response) => {
  const blog = await Blog.find({}).populate('user', {
    username: 1,
    name: 1,
    id: 1,
  })
  response.json(blog)
})

blogRouter.get('/:id', async (request, response) => {
  const { id } = request.params
  const blog = await Blog.findById(id)
  if (blog) {
    response.json(blog)
  } else {
    response.status(404).end()
  }
})

blogRouter.post('/', async (request, response) => {
  const blog = new Blog(request.body)
  const user = await User.findById(request.body.user)

  if (user) {
    if (!blog.likes) {
      blog.likes = 0
    }
    const blogSaved = await blog.save()
    user.blogs = user.blogs.concat(blogSaved._id)
    await user.save()
    response.status(201).json(blogSaved)
  } else {
    response.status(400).send({
      error: 'user not found',
    })
  }
})

blogRouter.delete('/:id', async (request, response) => {
  const { id } = request.params
  const blogDeleted = await Blog.findByIdAndRemove(id)
  if (blogDeleted) {
    response.status(204).end()
  } else {
    response.status(404).end()
  }
})

blogRouter.put('/:id', async (request, response) => {
  const { id } = request.params
  const blog = {
    ...request.body,
  }
  const blogUpdated = await Blog.findOneAndUpdate({ _id: id }, blog, { new: true, runValidators: true })
  if (blogUpdated) {
    response.json(blogUpdated)
  } else {
    response.status(404).end()
  }
})

module.exports = blogRouter
