require('dotenv').config()

const PORT = process.env.PORT || 3001
const MONGODBURI = process.env.NODE_ENV === 'test' ? process.env.MONGODBTEST_URI : process.env.MONGODB_URI

module.exports = {
  PORT,
  MONGODBURI,
}
