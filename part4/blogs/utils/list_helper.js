const dummy = (_blogs) => 1

const totalLikes = (blogs) => {
  let result
  if (!blogs || blogs.length < 1) {
    result = 0
  } else if (blogs.length === 1) {
    result = 1
  } else {
    result = blogs.map((b) => b.likes).reduce((a, b) => a + b)
  }
  return result
}

const favoriteBlog = (blogs) => {
  let result
  if (!blogs || blogs.length < 1) {
    result = {}
  } else if (blogs.length === 1) {
    result = { ...blogs[0] }
  } else {
    let maxLikes = 0
    blogs.forEach((b) => {
      if (b.likes > maxLikes) {
        result = {
          title: b.title,
          author: b.author,
          likes: b.likes,
        }
        maxLikes = b.likes
      }
    })
  }
  return result
}

module.exports = {
  dummy,
  totalLikes,
  favoriteBlog,
}
