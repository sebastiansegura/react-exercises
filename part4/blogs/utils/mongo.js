const mongoose = require('mongoose')
const { MONGODBURI } = require('./config')

const connectDb = async () => {
  await mongoose.connect(MONGODBURI)
}

connectDb()

module.exports = mongoose
