const express = require('express')
const cors = require('cors')
require('express-async-errors')

const app = express()
app.use(cors())
app.use(express.json())

const loginRouter = require('./controllers/login')
const userRouter = require('./controllers/user')
const blogRouter = require('./controllers/blog')
const { unknownEndpoint, errorHandler } = require('./utils/middleware')

app.use('/api/login', loginRouter)
app.use('/api/users', userRouter)
app.use('/api/blogs', blogRouter)

app.use(unknownEndpoint)
app.use(errorHandler)

module.exports = app
