const supertest = require('supertest')
const bcrypt = require('bcrypt')
const mongoose = require('../utils/mongo')
const User = require('../models/user')
const app = require('../app')

const api = supertest(app)

beforeEach(async () => {
  await User.deleteMany({})
  const saltRound = 10
  const passwordHash = await bcrypt.hash('password', saltRound)
  const user = new User({
    username: 'username',
    name: 'name',
    passwordHash,
  })
  await user.save()
})

describe('create user', () => {
  test('password invalid on create user', async () => {
    const user = {
      username: 'username1',
      name: 'name1',
      password: 'p',
    }

    const userSaved = await api
      .post('/api/users')
      .send(user)
      .expect(400)
      .expect('Content-Type', /application\/json/)

    expect(userSaved.error).toBeDefined()
  })

  test('user already exists', async () => {
    const user = {
      username: 'username',
      name: 'name',
      password: 'password',
    }

    const userSaved = await api
      .post('/api/users')
      .send(user)
      .expect(409)
      .expect('Content-Type', /application\/json/)

    expect(userSaved.error).toBeDefined()
    expect(userSaved.error.text).toContain('to be unique')
  })

  test('user valid', async () => {
    const user = {
      username: 'username1',
      name: 'name1',
      password: 'password1',
    }

    await api
      .post('/api/users')
      .send(user)
      .expect(201)
      .expect('Content-Type', /application\/json/)
  })
})

afterAll(async () => {
  mongoose.connection.close()
})
