const supertest = require('supertest')
const mongoose = require('../utils/mongo')
const app = require('../app')
const Blog = require('../models/blog')
const { initialBlogs } = require('../utils/test_helper')

const api = supertest(app)

beforeEach(async () => {
  await Blog.deleteMany({})
  const blogs = initialBlogs.map((blog) => new Blog(blog))
  const promiseSaveBlogs = blogs.map((blog) => blog.save())
  await Promise.all(promiseSaveBlogs)
})

describe('mongo', () => {
  test('get blogs', async () => {
    const { body: blogs } = await api.get('/api/blogs')
    expect(blogs).toHaveLength(5)
  })

  test('property id defined', async () => {
    const { body: blogs } = await api.get('/api/blogs')
    expect(blogs[0].id).toBeDefined()
  })

  test('save blog', async () => {
    const blog = {
      title: 'title',
      author: 'author',
      url: 'url',
      likes: 1,
    }

    await api
      .post('/api/blogs')
      .send(blog)
      .expect(201)
      .expect('Content-Type', /application\/json/)

    const { body: blogs } = await api.get('/api/blogs')
    expect(blogs).toHaveLength(initialBlogs.length + 1)
    const titles = blogs.map((b) => b.title)
    expect(titles).toContain('title')
  })

  test('verify likes property', async () => {
    const blog = {
      title: 'title',
      author: 'author',
      url: 'url',
    }

    await api
      .post('/api/blogs')
      .send(blog)
      .expect(201)
      .expect('Content-Type', /application\/json/)

    const { body: blogs } = await api.get('/api/blogs')
    const likes = blogs.map((b) => b.likes)
    expect(likes).toContain(0)
  })

  test('delete blog', async () => {
    const { body: blogsAtStart } = await api.get('/api/blogs')
    const blogToDelete = blogsAtStart[0]
    await api.delete(`/api/blogs/${blogToDelete.id}`).expect(204)
    const { body: blogsAtEnd } = await api.get('/api/blogs')
    expect(blogsAtEnd).toHaveLength(blogsAtStart.length - 1)
    const titles = blogsAtEnd.map((b) => b.title)
    expect(titles).not.toContain(blogToDelete.title)
  })

  test('update blog', async () => {
    const { body: blogsAtStart } = await api.get('/api/blogs')
    const blogToUpdate = blogsAtStart[0]
    const blog = {
      title: 'title0',
    }
    await api
      .put(`/api/blogs/${blogToUpdate.id}`)
      .send(blog)
      .expect(200)
      .expect('Content-Type', /application\/json/)
    const { body: blogsAtEnd } = await api.get('/api/blogs')
    expect(blogsAtEnd).toHaveLength(blogsAtStart.length)
    const titles = blogsAtEnd.map((b) => b.title)
    expect(titles).toContain(blog.title)
  })
})

afterAll(() => {
  mongoose.connection.close()
})
