const mongoose = require('../utils/mongo')
const listHelper = require('../utils/list_helper')
const { initialBlogs } = require('../utils/test_helper')

describe('dummy', () => {
  test('dummy returns one', () => {
    const result = listHelper.dummy([])
    expect(result).toBe(1)
  })
})

describe('total likes', () => {
  test('of empty list is zero', () => {
    const result = listHelper.totalLikes([])
    expect(result).toBe(0)
  })
  test('when list has only one blog, equals the likes of that', () => {
    const result = listHelper.totalLikes([initialBlogs[0]])
    expect(result).toBe(1)
  })
  test('of a bigger list is calculated right', () => {
    const result = listHelper.totalLikes(initialBlogs)
    expect(result).toBe(36)
  })
})

describe('favorite blog', () => {
  test('of empty list is zero', () => {
    const result = listHelper.favoriteBlog([])
    expect(result).toEqual({})
  })
  test('when list has only one blog, equals the likes of that', () => {
    const result = listHelper.favoriteBlog([initialBlogs[0]])
    expect(result).toEqual({
      title: 'React patterns',
      author: 'Michael Chan',
      url: 'https://reactpatterns.com/',
      likes: 7,
    })
  })
  test('of a bigger list is calculated right', () => {
    const result = listHelper.favoriteBlog(initialBlogs)
    expect(result).toEqual({
      title: 'Canonical string reduction',
      author: 'Edsger W. Dijkstra',
      likes: 12,
    })
  })
})

afterAll(() => {
  mongoose.connection.close()
})
