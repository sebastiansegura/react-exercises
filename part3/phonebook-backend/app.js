require('dotenv').config()
const express = require('express')
const cors = require('cors')
const personRouter = require('./controller/person')
const { unknownEndpoint, errorHandler } = require('./utils/middleware')

const app = express()
app.use(express.json())
app.use(cors())
app.use(express.static('dist'))
app.use('/api/persons', personRouter)
app.use(unknownEndpoint)
app.use(errorHandler)

module.exports = app
