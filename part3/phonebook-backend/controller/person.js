const personRouter = require('express').Router()
const morgan = require('morgan')
const Person = require('../models/person')

morgan.token('body', (req) => JSON.stringify(req.body))
personRouter.use(morgan(':method :url :status :res[content-length] - :response-time ms :body'))

personRouter.get('/info', (request, response, next) => {
  Person.count({})
    .then((result) => response.send(`<p>Phonebook has info for ${result} people</p><p>${new Date()}</p>`))
    .catch((error) => next(error))
})

personRouter.get('/', (request, response, next) => {
  Person.find({})
    .then((persons) => {
      response.json(persons)
    })
    .catch((error) => next(error))
})

personRouter.get('/:id', (request, response, next) => {
  const { id } = request.params
  Person.findById(id)
    .then((person) => {
      if (person) {
        response.json(person)
      } else {
        response.status(404).json({
          error: 'Person not found',
        })
      }
    })
    .catch((error) => next(error))
})

personRouter.delete('/:id', (request, response, next) => {
  const { id } = request.params
  Person.findByIdAndRemove(id)
    .then((result) => {
      if (!result) {
        return response.status(404).json({
          error: 'Person not found',
        })
      }
      return response.status(200).end()
    })
    .catch((error) => next(error))
})

personRouter.post('/', (request, response, next) => {
  const { body } = request
  Person.find({ number: body.number })
    .then((result) => {
      if (result.length > 0) {
        response.status(409).json({
          error: 'number already registered',
        })
      }
    })
    .catch((error) => next(error))
  const person = new Person({
    name: body.name,
    number: body.number,
  })
  person
    .save()
    .then((savedPerson) => savedPerson.toJSON())
    .then((formattedPerson) => response.status(201).json(formattedPerson))
    .catch((error) => next(error))
})

personRouter.put('/:id', (request, response, next) => {
  const { id } = request.params
  const { body } = request
  const person = {
    name: body.name,
    number: body.number,
  }
  Person.findOneAndUpdate({ _id: id }, person, { new: true, runValidators: true })
    .then((updatedPerson) => {
      if (!updatedPerson) {
        return response.status(404).json({
          error: 'Person not found',
        })
      }
      return updatedPerson.toJSON()
    })
    .then((formattedPerson) => {
      response.json(formattedPerson)
    })
    .catch((error) => next(error))
})

module.exports = personRouter
