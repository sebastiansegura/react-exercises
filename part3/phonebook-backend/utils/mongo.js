const mongoose = require('mongoose')

mongoose
  .connect(process.env.MONGODB_URI)
  .then((_result) => console.log('connected to MongoDB'))
  .catch((error) => console.log('error connecting to MongoDB:', error.message))

module.exports = mongoose
