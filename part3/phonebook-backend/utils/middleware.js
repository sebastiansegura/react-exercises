const unknownEndpoint = (request, response) => {
  response.status(404).send({
    error: 'unknown endpoint',
  })
}

const errorHandler = (error, request, response, next) => {
  if (error.name === 'CastError') {
    response.status(400).json({
      error: 'malformed ID',
    })
  } else if (error.name === 'ValidationError') {
    if (error.message.includes('to be unique')) {
      response.status(409).json({
        error: error.message,
      })
    } else {
      response.status(400).json({
        error: error.message,
      })
    }
  } else {
    response.status(500).json({
      error: error.message,
    })
  }
  next(error)
}

module.exports = { unknownEndpoint, errorHandler }
